﻿using MyFirstProgram;
using System;
namespace GameTools
{

    /*
     * GameEngine:
     * Protype to print sequence of frames in console.
     * 
     * To stop de main while press ESC key.
     * 
     */


    public class GameEngine
    {

        //Declaració d'una variable
        private ConsoleColor _backgroundConsoleColor;
        //Declaració d'una propietat
        public ConsoleColor BackgroundConsoleColor
        {
            get { return _backgroundConsoleColor; }
            set
            {
                if (ConsoleColor.White != value || ConsoleColor.Black != value) _backgroundConsoleColor = value;
                else
                {
                    _backgroundConsoleColor = ConsoleColor.Gray;
                    throw new ArgumentException($"Console color {value} not recomended. Set by default");
                }
            }
        }
        
        private int time2liveframe;
        private float _frameRate;
        public float FrameRate
        {
            get { return _frameRate; }
            set
            {
                //Ternari condition
                _frameRate = (value < 0f) ? value * (-1f) : value;  
            }
        }


        //Declaració d'una propietat no protegida
        public int Frames { get; set; }

        private ConsoleKeyInfo cki;
        private bool engineSwitch;

        public GameEngine()
        {
            InitGame();
            UpdateGame();
            CloseGame();
        }


        private void InitGame()
        {
            /*** Init variables ***/

            Frames = 0;
            engineSwitch = true;

            //Acces exemple with this:
            this._frameRate = (_frameRate <= 0) ? 12: _frameRate;

            //Calculate the frame time in miliseconds. Time to refresh. F=1/s ->s=1/F
            time2liveframe = (int)((1 / _frameRate)*1000);

            /*******/

            //Prepare Console
            CleanFrame();
            Console.BackgroundColor = _backgroundConsoleColor;

            Console.WriteLine($"Game Initiation             Render data: Framerate: {_frameRate} || TimeToRefresh:{time2liveframe}");

            Start();

            System.Threading.Thread.Sleep(2000);
        }

        /*
         * Engine updates every frame
         * 
         * Reprint console
         */
        private void UpdateGame()
        {

            do
            {
                CleanFrame();

                Console.WriteLine($"Frame number {Frames}");

                Update();

                RefreshFrame();

                CheckKeyboard4Engine();

                Frames++;

            } while (engineSwitch);
            
        }

        private void CheckKeyboard4Engine()
        {
            engineSwitch = (cki.Key != ConsoleKey.Escape);
        }

        private void RefreshFrame()
        {
            //Access to Threading library only in this line
            System.Threading.Thread.Sleep(time2liveframe);
        }

        private void CloseGame()
        {
            Exit();
            Console.WriteLine(" Game Over. Closing game");
        }


        private void CleanFrame()
        {
            Console.Clear();
            Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop);
        }

        //Variables
        MatrixRepresentation matrix = new MatrixRepresentation();
        Random rnd = new Random();
        Random rndLetter = new Random();
        int row = 0;
        int column;
        int letterNum;
        char letter;
        int input;

        protected void Start()
        {
            //Code before first frame
            matrix.CleanTheMatrix();
            column = getColumn();
            letterNum = rndLetter.Next(1, 24);

        }

        private int getColumn()
        {
            Console.WriteLine("Columna on deixar caure una lletra:");
            input = int.Parse(Console.ReadLine());
            while (input >= 9 || input < 0)
            {
                Console.WriteLine("La columna ha d'estar entre 0 i 8");
                input = int.Parse(Console.ReadLine());
            }
            return input;
        }

        protected void Update()
        {
            //Execution ontime secuence of every frame
            letterNum = getLetter();
            column = charFall(column, letter);
            matrix.printMatrix();
        }

        private int getLetter()
        {
            letter = (char)(letterNum + 64);
            if (row < matrix.TheMatrix.GetLength(0) - 1)
            {
                return letterNum;
            }
            else
            {
                letter = (char)(letterNum + 64);
                letterNum = rndLetter.Next(1, 24);
                return letterNum;
            }
        }

        private int charFall(int column, char letter)
        {   
            if (row == 0)
            {
                for (int i = 0; i < matrix.TheMatrix.GetLength(1); ++i)
                {
                    matrix.TheMatrix[matrix.TheMatrix.GetLength(0) - 1, i] = '0';
                }
            } else
            {
                matrix.TheMatrix[row - 1, column] = '0';
            }
            
            if (row < matrix.TheMatrix.GetLength(0) - 1)
            {
                matrix.TheMatrix[row, column] = letter;
                row++;
            } else
            {
                row = 0;
                return getColumn();
            }
            return column;
        }

        protected void Exit()
        {
            //Code afer last frame
        }

    }
}


public class ConsoleSpiner
{
    int counter;
    public ConsoleSpiner()
    {
        counter = 0;
    }
    public void Turn()
    {
        counter++;
        switch (counter % 4)
        {
            case 0: Console.Write("/"); break;
            case 1: Console.Write("-"); break;
            case 2: Console.Write("\\"); break;
            case 3: Console.Write("|"); break;
        }
        //Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
    }
}
